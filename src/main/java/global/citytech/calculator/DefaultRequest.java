package global.citytech.calculator;

public class DefaultRequest implements Request {

    private double valueOne;
    private double valueTwo;


    public DefaultRequest(double valueOne, double valueTwo) {
        this.valueOne = valueOne;
        this.valueTwo = valueTwo;
    }

    public double getValueOne() { return valueOne; }

    public void setValueOne(double valueOne) {
        this.valueOne = valueOne;
    }

    public double getValueTwo() {
        return valueTwo;
    }

    public void setValueTwo(double valueTwo) {
        this.valueTwo = valueTwo;
    }
}