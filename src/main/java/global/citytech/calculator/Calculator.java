package global.citytech.calculator;

import global.citytech.calculator.impl.*;

import java.util.HashMap;
import java.util.Map;

public class Calculator {

    private Map<String, CalculatorService> map;

    public Calculator() {
        this.map = new HashMap<>();
        this.init();
    }

    private void init() {
        this.map.putIfAbsent("add", new AddService());
        this.map.putIfAbsent("subtract", new SubtractService());
        this.map.putIfAbsent("multiply", new MultiplyService());
        this.map.putIfAbsent("divide", new DivideService());
        this.map.putIfAbsent("squareRoot", new SquareRootService());
        this.map.putIfAbsent("squareOf", new SquareService());
    }

    public double calculate(Request request, String operation) {
        CalculatorService service = this.map.get(operation);
        if (service != null) {
            return service.calculate(request);
        }
        throw new IllegalArgumentException("Invalid Calc Operation");
    }
}