package global.citytech.calculator;

import javax.swing.*;
public class CalculatorTest {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        String[] options = new String[]{"add", "subtract","multiply", "divide","squareOf", "squareRoot"};
        String input = (String) JOptionPane.showInputDialog(null, "Choose Operation...", " Operation", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        if (containsOperation(input)){

            double num = Double.parseDouble(JOptionPane.showInputDialog("Enter a number : "));
            SquareRequest squareRequest = new SquareRequest(num);
            double result = calculator.calculate(squareRequest, input);
            JOptionPane.showMessageDialog(null, " Result is : " + result);

        } else {

            double  num1 = Double.parseDouble(JOptionPane.showInputDialog("Enter first number : "));
            double num2 = Double.parseDouble(JOptionPane.showInputDialog("Enter second number : "));
            DefaultRequest defaultRequest = new DefaultRequest(num1, num2);
            double result = calculator.calculate(defaultRequest,input);
            JOptionPane.showMessageDialog(null," Result is : " +result);

        }
    }

    public static boolean containsOperation(String operation) {
        if(operation != "" && operation != null) {
            String[] ops = {"squareOf", "squareRoot"};
            for (int i = 0; i < ops.length; i++) {
                if (operation.equalsIgnoreCase(ops[i])) {
                    return true;
                }
            }
        }
        return false;
    }
}
