package global.citytech.calculator;

public class DefaultResponse implements Response {

    private final double result;

    public DefaultResponse(double result) {
        this.result = result;
    }

    public double getResult() {
        return result;
    }
}