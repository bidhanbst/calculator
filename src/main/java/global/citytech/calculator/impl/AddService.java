package global.citytech.calculator.impl;

import global.citytech.calculator.*;

public class AddService implements CalculatorService {
    public double calculate(Request request) {
        DefaultRequest addRequest = (DefaultRequest) request;
        return addRequest.getValueOne() + addRequest.getValueTwo();
    }
}
