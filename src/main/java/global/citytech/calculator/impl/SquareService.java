package global.citytech.calculator.impl;

import global.citytech.calculator.CalculatorService;
import global.citytech.calculator.Request;
import global.citytech.calculator.SquareRequest;

public class SquareService implements CalculatorService {
    public double calculate(Request request) {
        SquareRequest squareRequest = (SquareRequest) request;
        return Math.pow(squareRequest.getNumber(), 2);
    }
}
