package global.citytech.calculator.impl;

import global.citytech.calculator.CalculatorService;
import global.citytech.calculator.Request;
import global.citytech.calculator.SquareRequest;

public class SquareRootService implements CalculatorService {
    public double calculate(Request request) {
        SquareRequest squareRootRequest = (SquareRequest) request;
        return Math.sqrt(squareRootRequest.getNumber());
    }
}




