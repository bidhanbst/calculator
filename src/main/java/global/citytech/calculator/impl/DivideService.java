package global.citytech.calculator.impl;

import global.citytech.calculator.CalculatorService;
import global.citytech.calculator.DefaultRequest;
import global.citytech.calculator.Request;

public class DivideService implements CalculatorService {
    public double calculate(Request request) {
        DefaultRequest divide = (DefaultRequest) request;
        return divide.getValueOne() / divide.getValueTwo();
    }
}
