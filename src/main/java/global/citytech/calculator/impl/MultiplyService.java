package global.citytech.calculator.impl;

import global.citytech.calculator.CalculatorService;
import global.citytech.calculator.DefaultRequest;
import global.citytech.calculator.Request;

public class MultiplyService implements CalculatorService {

    public double calculate(Request request) {
        DefaultRequest multiply = (DefaultRequest) request;
        return multiply.getValueOne() * multiply.getValueTwo();
    }
}
