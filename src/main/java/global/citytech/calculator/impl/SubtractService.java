package global.citytech.calculator.impl;

import global.citytech.calculator.CalculatorService;
import global.citytech.calculator.DefaultRequest;
import global.citytech.calculator.Request;

public class SubtractService implements CalculatorService {
    public double calculate(Request request) {
        DefaultRequest addRequest = (DefaultRequest) request;
        return addRequest.getValueOne() - addRequest.getValueTwo();
    }
}
