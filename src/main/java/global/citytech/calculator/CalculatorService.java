package global.citytech.calculator;

public interface CalculatorService {

    double calculate(Request request);
}
