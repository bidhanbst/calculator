package global.citytech.calculator;

public class SquareRequest implements Request {
    private final double number;

    public SquareRequest(double number) {
        this.number = number;
    }

    public double getNumber() {
        return number;
    }
}
